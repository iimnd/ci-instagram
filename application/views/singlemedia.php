<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>instagram api
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="http://bootstraptaste.com" />
    <!-- css -->
    <link href='<?php echo base_url();?>assets/css/bootstrap.min.css' rel="stylesheet" />
    <link href='<?php echo base_url();?>assets/css/fancybox/jquery.fancybox.css' rel="stylesheet">
    <link href='<?php echo base_url();?>assets/css/jcarousel.css' rel="stylesheet" />
    <link href='<?php echo base_url();?>assets/css/flexslider.css' rel="stylesheet" />
    <link href='<?php echo base_url();?>assets/css/style.css' rel="stylesheet" />
    <!-- Theme skin -->
    <link href="<?php echo base_url();?>assets/skins/default.css" rel="stylesheet" />
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
  </head>
  <body>
    <div id="wrapper">
      <!-- start header -->
      <header>
        <div class="navbar navbar-default navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar">
                </span>
                <span class="icon-bar">
                </span>
                <span class="icon-bar">
                </span>
              </button>
              <a class="navbar-brand" href="index.html">
                <span>I
                </span>nstagram
              </a>
            </div>
            <div class="navbar-collapse collapse ">
              <ul class="nav navbar-nav">
                <li class="active">
                  <a href="index.html">Home
                  </a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Features 
                    <b class=" icon-angle-down">
                    </b>
                  </a>
                  <ul class="dropdown-menu">
                    <li>
                      <a href="typography.html">Typography
                      </a>
                    </li>
                    <li>
                      <a href="components.html">Components
                      </a>
                    </li>
                    <li>
                      <a href="pricingbox.html">Pricing box
                      </a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="portfolio.html">Portfolio
                  </a>
                </li>
                <li>
                  <a href="blog.html">Blog
                  </a>
                </li>
                <li>
                  <a href="contact.html">Contact
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </header>
      <!-- end header -->
      <section id="content">
        <div class="container">
          <h1>List User
          </h1>
          <div class="row">
            <div class="col-lg-12">
              <div class="row">
                <?php 
$tipe= $links['data']['type'];
if($tipe=='video'){
$url = $links['data']['videos']['standard_resolution']['url'];
} else{
$url = $links['data']['images']['standard_resolution']['url'];
}
?>
                <div class="col-lg-3">
                  <div class="box">
                    <div class="box-gray aligncenter">
                      <h4>
                        <?php echo $tipe;?>
                      </h4>
                      <div class="icon">
                        <?php 
echo '<a href = "'.$url.'" target="blank"><img style="width:100px; height:100px;"src ="'.$url.'"/></a>'.'<br />';
?>
                      </div>
                      <p>
                        <?php 
echo 'Total Comment : '.$links['data']['comments']['count'].'<br />';
echo 'Created On : '.date('Y-m-j H:i:s', $links['data']['created_time']).'<br />';
echo 'Total like : '.$links['data']['likes']['count'].'<br />';
foreach ($links['data']['users_in_photo'] as  $usertag) {
echo 'Users in Photo: '.$usertag['user']['username'].' | ';
echo 'ID : '.$usertag['user']['id'].' | ';
echo 'Fullname : '.$usertag['user']['full_name'].' | '.'<br /><br />';
}
echo 'Caption : '.$links['data']['caption']['text'].'<br />';
echo 'Created By : '.$links['data']['caption']['from']['username'].' | ';
echo 'ID : '.$links['data']['caption']['from']['id'].' | ';
echo 'Fullname : '.$links['data']['caption']['from']['full_name'].' | ';
echo '<a href = "'.$links['data']['caption']['from']['profile_picture'].'" target="blank"><img style="width:30px; height:30px;"src ="'.$links['data']['caption']['from']['profile_picture'].'"/></a>'.'<br />';
?>
                      </p>
                    </div>
                    <div class="box-bottom">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- divider -->
          <div class="row">
            <div class="col-lg-12">
              <div class="solidline">
              </div>
            </div>
          </div>
          <!-- end divider -->
        </div>
      </section>
    </div>
    <a href="#" class="scrollup">
      <i class="fa fa-angle-up active">
      </i>
    </a>
    <!-- javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src='<?php echo base_url();?>assets/js/jquery.js'>
    </script>
    <script src='<?php echo base_url();?>assets/js/jquery.easing.1.3.js'>
    </script>
    <script src='<?php echo base_url();?>assets/js/bootstrap.min.js'>
    </script>
    <script src='<?php echo base_url();?>assets/js/jquery.fancybox.pack.js'>
    </script>
    <script src='<?php echo base_url();?>assets/js/jquery.fancybox-media.js'>
    </script>
    <script src='<?php echo base_url();?>assets/js/google-code-prettify/prettify.js'>
    </script>
    <script src='<?php echo base_url();?>assets/js/portfolio/jquery.quicksand.js'>
    </script>
    <script src='<?php echo base_url();?>assets/js/portfolio/setting.js'>
    </script>
    <script src='<?php echo base_url();?>assets/js/jquery.flexslider.js'>
    </script>
    <script src='<?php echo base_url();?>assets/js/animate.js'>
    </script>
    <script src='<?php echo base_url();?>assets/js/custom.js'>
    </script>
  </body>
</html>
