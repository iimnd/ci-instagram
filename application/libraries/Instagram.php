<?php
/*==========================================
Create by : IIM NUR DIANSYAH
Bitbucket : https://bitbucket.org/iimnd/
============================================*/
class Instagram

	{
	public $access_token = '1388393123.9fc6aaf.2b91682dd4f4411f9569b296732bf54b';

	function deleteComment($media_id, $comment_id)
		{
		$url = 'https://api.instagram.com/v1/media/' . $media_id . '/comments' . '/' . $comment_id . '?access_token=' . $this->access_token . '';
		try
			{
			$curl_connection = curl_init($url);
			curl_setopt($curl_connection, CURLOPT_CUSTOMREQUEST, 'DELETE');
			curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
			// Data are stored in $data
			$data = json_decode(curl_exec($curl_connection) , true);
			// print_r($data);
			curl_close($curl_connection);
			return "Sukses";
			}
		catch(Exception $e)
			{
			return $e->getMessage();
			}
		}
	function unlikePost($media_id)
		{
		$url = 'https://api.instagram.com/v1/media/' . $media_id . '/likes' . '?access_token=' . $this->access_token . '';
		try
			{
			$curl_connection = curl_init($url);
			curl_setopt($curl_connection, CURLOPT_CUSTOMREQUEST, 'DELETE');
			curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
			// Data are stored in $data
			$data = json_decode(curl_exec($curl_connection) , true);
			// print_r($data);
			curl_close($curl_connection);
			return "Sukses Unlike Post";
			}
		catch(Exception $e)
			{
			return $e->getMessage();
			}
		}
	function getComment($media_id)
		{
		$json_url = 'https://api.instagram.com/v1/media/' . $media_id . '/comments?access_token=' . $this->access_token . '';
		$json = file_get_contents($json_url);
		$links = json_decode($json);
		return $links->data;
		/*
		foreach($links->data as $datas)
		{
		echo 'ID : ' . $datas->id . '<br />';
		echo 'Created On : ' . date('Y-m-j H:i:s', $datas->created_time) . '<br />';
		echo 'Comment : ' . $datas->text . '<br />';
		echo 'Created By : ' . $datas->from->username . ' | ';
		echo 'ID : ' . $datas->from->id . ' | ';
		echo 'Full Name : ' . $datas->from->full_name . ' | ';
		echo '<a href = "' . $datas->from->profile_picture . '" target="blank"><img style="width:30px; height:30px;" src ="' . $datas->from->profile_picture . '"/></a>' . '<br /><br />';
		}*/
		}
	function getMyFollower()
		{
		$json_url = 'https://api.instagram.com/v1/users/self/followed-by?access_token=' . $this->access_token . '';
		$json = file_get_contents($json_url);
		$links = json_decode($json);
		return $links->data;
		/*
		foreach( $links->data as $datas ) {
		echo '<br /> <a href = "'.$datas->profile_picture.'" target="blank"><img style="width:90px; height:90px;" src ="'.$datas->profile_picture.'"/></a>'.'<br /><br />';
		echo 'Username :'.$datas->username.' <br /> ';
		echo 'ID :'.$datas->id.' <br /> ';
		echo 'Full Name :'.$datas->full_name.' <br /> ';
		}*/
		}
	function getMyFollowing()
		{
		$json_url = 'https://api.instagram.com/v1/users/self/follows?access_token=' . $this->access_token . '';
		$json = file_get_contents($json_url);
		$links = json_decode($json);
		return $links->data;
		/*
		foreach( $links->data as $datas ) {
		echo '<br /> <a href = "'.$datas->profile_picture.'" target="blank"><img style="width:90px; height:90px;" src ="'.$datas->profile_picture.'"/></a>'.'<br /><br />';
		echo 'Username :'.$datas->username.' <br /> ';
		echo 'ID :'.$datas->id.' <br /> ';
		echo 'Full Name :'.$datas->full_name.' <br /> ';
		}*/
		}
	function getLikerByMediaId($media_id)
		{
		$json_url = 'https://api.instagram.com/v1/media/' . $media_id . '/likes?access_token=' . $this->access_token . '';
		$json = file_get_contents($json_url);
		$links = json_decode($json);
		return $links->data;
		/*
		foreach( $links->data as $datas ) {
		echo '<a href = "'.$datas->profile_picture.'" target="blank"><img src ="'.$datas->profile_picture.'"/></a>'.'<br />';
		echo 'Account ID :'.$datas->id.'<br />';
		echo 'Username :'.$datas->username.'<br />';
		echo 'Full Name:'.$datas->full_name.'<br />';
		}*/
		}
	function getLocationInfoById($loc_id)
		{
		$json_url = 'https://api.instagram.com/v1/locations/' . $loc_id . '?access_token=' . $this->access_token . '';
		$json = file_get_contents($json_url);
		$links = json_decode($json, true);
		return $links;
		/*
		echo $links['data']['id'].'<br />';
		echo $links['data']['name'].'<br />';
		echo $links['data']['latitude'].'<br />';
		echo $links['data']['longitude'].'<br />';*/
		}
	function getMediaByLatLng($lat, $lng)
		{
		$json_url = 'https://api.instagram.com/v1/media/search?lat=' . $lat . '&lng=' . $lng . '&access_token=' . $this->access_token . '';
		$json = file_get_contents($json_url);
		$links = json_decode($json);
		return $links->data;
		/*
		foreach( $links->data as $datas ) {
		if($datas->type=='video'){
		$url = $datas->videos->standard_resolution->url;
		} else{
		$url = $datas->images->standard_resolution->url;
		}
		echo '<a href = "'.$url.'" target="blank"><img src ="'.$datas->images->thumbnail->url.'"/></a>'.'<br />';
		echo 'Tipe :'.$datas->type.'<br />';
		echo 'Like :'.$datas->likes->count.'<br />';
		echo 'Caption:'.$datas->caption->text.'<br />';
		echo 'Post ID :'.$datas->id.'<br />';
		if($datas->location!=NULL){
		echo 'Locations Info: '.' ID : '.$datas->location->id.' | ';
		echo ' Name : '.$datas->location->name.' | ';
		echo ' Latitude : '.$datas->location->latitude.' | ';
		echo ' Longitude : '.$datas->location->longitude.' | <br />';
		echo 'Created On : '.date('Y-m-j H:i:s', $datas->created_time).'<br />';
		$tagarray=$datas->tags;
		}
		echo 'Tags: '.implode(", ", $tagarray).'<br />';
		foreach ($datas->users_in_photo as  $usertag) {
		echo 'Users in Photo: '.$usertag->user->username.' | ';
		echo 'ID : '.$usertag->user->id.' | ';
		echo 'Fullname : '.$usertag->user->full_name.' | '.'<br /><br />';
		}
		echo 'Created By :'.$datas->caption->from->username.' | ';
		echo 'ID :'.$datas->caption->from->id.' | ';
		echo 'Fullname :'.$datas->caption->from->full_name.' | ';
		echo '<a href = "'.$datas->caption->from->profile_picture.'" target="blank"><img style="width:30px; height:30px;" src ="'.$datas->caption->from->profile_picture.'"/></a>'.'<br /><br />';
		}*/
		}
	function getRecentMediaByTag($tag)
		{
		$json_url = 'https://api.instagram.com/v1/tags/' . $tag . '/media/recent?access_token=' . $this->access_token . '';
		$json = file_get_contents($json_url);
		$links = json_decode($json);
		return $links->data;
		/*
		foreach( $links->data as $datas ) {
		if($datas->type=='video'){
		$url = $datas->videos->standard_resolution->url;
		} else{
		$url = $datas->images->standard_resolution->url;
		}
		echo '<a href = "'.$url.'" target="blank"><img src ="'.$datas->images->thumbnail->url.'"/></a>'.'<br />';
		echo 'Tipe :'.$datas->type.'<br />';
		echo 'Like :'.$datas->likes->count.'<br />';
		echo 'Caption:'.$datas->caption->text.'<br />';
		echo 'Post ID :'.$datas->id.'<br />';
		if($datas->location!=NULL){
		echo 'Locations Info: '.' ID : '.$datas->location->id.' | ';
		echo ' Name : '.$datas->location->name.' | ';
		echo ' Latitude : '.$datas->location->latitude.' | ';
		echo ' Longitude : '.$datas->location->longitude.' | <br />';
		echo 'Created On : '.date('Y-m-j H:i:s', $datas->created_time).'<br />';
		$tagarray=$datas->tags;
		}
		echo 'Tags: '.implode(", ", $tagarray).'<br />';
		foreach ($datas->users_in_photo as  $usertag) {
		echo 'Users in Photo: '.$usertag->user->username.' | ';
		echo 'ID : '.$usertag->user->id.' | ';
		echo 'Fullname : '.$usertag->user->full_name.' | '.'<br /><br />';
		}
		echo 'Created By :'.$datas->caption->from->username.' | ';
		echo 'ID :'.$datas->caption->from->id.' | ';
		echo 'Fullname :'.$datas->caption->from->full_name.' | ';
		echo '<a href = "'.$datas->caption->from->profile_picture.'" target="blank"><img style="width:30px; height:30px;" src ="'.$datas->caption->from->profile_picture.'"/></a>'.'<br /><br />';
		}*/
		}
	function getRecentMediaByLocation($loc_id)
		{
		$json_url = 'https://api.instagram.com/v1/locations/' . $loc_id . '/media/recent?access_token=' . $this->access_token . '';
		$json = file_get_contents($json_url);
		$links = json_decode($json);
		return $links->data;
		/*
		foreach( $links->data as $datas ) {
		if($datas->type=='video'){
		$url = $datas->videos->standard_resolution->url;
		} else{
		$url = $datas->images->standard_resolution->url;
		}
		echo '<a href = "'.$url.'" target="blank"><img src ="'.$datas->images->thumbnail->url.'"/></a>'.'<br />';
		echo 'Tipe :'.$datas->type.'<br />';
		echo 'Like :'.$datas->likes->count.'<br />';
		echo 'Caption:'.$datas->caption->text.'<br />';
		echo 'Post ID :'.$datas->id.'<br />';
		echo 'Created On : '.date('Y-m-j H:i:s', $datas->created_time).'<br />';
		$tagarray=$datas->tags;
		echo 'Tags: '.implode(", ", $tagarray).'<br />';
		foreach ($datas->users_in_photo as  $usertag) {
		echo 'Users in Photo: '.$usertag->user->username.' | ';
		echo 'ID : '.$usertag->user->id.' | ';
		echo 'Fullname : '.$usertag->user->full_name.' | '.'<br /><br />';
		}
		echo 'Created By :'.$datas->caption->from->username.' | ';
		echo 'ID :'.$datas->caption->from->id.' | ';
		echo 'Fullname :'.$datas->caption->from->full_name.' | ';
		echo '<a href = "'.$datas->caption->from->profile_picture.'" target="blank"><img style="width:30px; height:30px;" src ="'.$datas->caption->from->profile_picture.'"/></a>'.'<br /><br />';
		echo 'Locations Info: '.' ID : '.$datas->location->id.' | ';
		echo ' Name : '.$datas->location->name.' | ';
		echo ' Latitude : '.$datas->location->latitude.' | ';
		echo ' Longitude : '.$datas->location->longitude.' | <br />';
		}*/
		}
	function getMyInfo()
		{
		$json_url = 'https://api.instagram.com/v1/users/self/?access_token=' . $this->access_token . '';
		$json = file_get_contents($json_url);
		$links = json_decode($json, true);
		return $links;
		/*
		echo $links['meta']['code'].'<br />';
		echo $links['data']['username'].'<br />';
		echo $links['data']['bio'].'<br />';
		echo $links['data']['website'].'<br />';
		echo '<a href = "'.$links['data']['profile_picture'].'" target="blank"><img src ="'.$links['data']['profile_picture'].'"/></a>'.'<br />';
		echo $links['data']['full_name'].'<br />';
		echo 'Total Media : '.$links['data']['counts']['media'].'<br />';
		echo 'Total Followers : '.$links['data']['counts']['followed_by'].'<br />';
		echo 'Total Following : '.$links['data']['counts']['follows'].'<br />';
		echo 'ID User : '.$links['data']['id'].'<br />'; */
		}
	function getUserInfoById($user_id)
		{
		$json_url = 'https://api.instagram.com/v1/users/' . $user_id . '/?access_token=' . $this->access_token . '';
		$json = file_get_contents($json_url);
		$links = json_decode($json, true);
		return $links;
		/*
		echo $links['meta']['code'].'<br />';
		echo $links['data']['username'].'<br />';
		echo $links['data']['bio'].'<br />';
		echo $links['data']['website'].'<br />';
		echo '<a href = "'.$links['data']['profile_picture'].'" target="blank"><img src ="'.$links['data']['profile_picture'].'"/></a>'.'<br />';
		echo $links['data']['full_name'].'<br />';
		echo 'Total Media : '.$links['data']['counts']['media'].'<br />';
		echo 'Total Followers : '.$links['data']['counts']['followed_by'].'<br />';
		echo 'Total Following : '.$links['data']['counts']['follows'].'<br />';
		echo 'ID User : '.$links['data']['id'].'<br />'; */
		}
	function likePost($media_id)
		{
		$url = 'https://api.instagram.com/v1/media/' . $media_id . '/likes';
		try
			{
			$curl_connection = curl_init($url);
			curl_setopt($curl_connection, CURLOPT_POST, true);
			curl_setopt($curl_connection, CURLOPT_POSTFIELDS, http_build_query(array(
				'access_token' => "$this->access_token"
			)));
			curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
			// Data are stored in $data
			$data = json_decode(curl_exec($curl_connection) , true);
			// print_r($data);
			curl_close($curl_connection);
			return "Sukses like Post";
			}
		catch(Exception $e)
			{
			return $e->getMessage();
			}
		}
	function postComment($comment, $media_id)
		{
		$url = 'https://api.instagram.com/v1/media/' . $media_id . '/comments';
		try
			{
			$curl_connection = curl_init($url);
			curl_setopt($curl_connection, CURLOPT_POST, true);
			curl_setopt($curl_connection, CURLOPT_POSTFIELDS, http_build_query(array(
				'access_token' => "$this->access_token",
				'text' => "$comment"
			)));
			curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
			// Data are stored in $data
			$data = json_decode(curl_exec($curl_connection) , true);
			// print_r($data);
			curl_close($curl_connection);
			return "Sukses Memberi Komentar";
			}
		catch(Exception $e)
			{
			return $e->getMessage();
			}
		}
	function countTag($tag)
		{
		$json_url = 'https://api.instagram.com/v1/tags/search?q=' . $tag . '&access_token=' . $this->access_token . '';
		$json = file_get_contents($json_url);
		$links = json_decode($json);
		return $links->data;
		/*
		foreach( $links->data as $datas ) {
		echo 'Jumlah Total Tag: '.$datas->media_count.'<br />';
		echo 'Name : '.$datas->name.'<br /> <br />';
		}*/
		}
	function searchLocationByLatLng($lat, $lng)
		{
		// 	Default is 500m (distance=500), max distance is 750.
		$json_url = 'https://api.instagram.com/v1/locations/search?lat=' . $lat . '&lng=' . $lng . '&access_token=' . $this->access_token . '';
		$json = file_get_contents($json_url);
		$links = json_decode($json);
		return $links->data;
		/*
		foreach( $links->data as $datas ) {
		echo 'Location ID :'.$datas->id.'<br />';
		echo 'Latitude :'.$datas->latitude.'<br />';
		echo 'Longitude :'.$datas->longitude.'<br />';
		echo 'Name:'.$datas->name.'<br /> <br />';
		}*/
		}
	function searchUser($username)
		{
		$json_url = 'https://api.instagram.com/v1/users/search?q=' . $username . '&access_token=' . $this->access_token . '';
		$json = file_get_contents($json_url);
		$links = json_decode($json);
		return $links->data;
		/*
		foreach( $links->data as $datas ) {
		echo '<a href = "'.$datas->profile_picture.'" target="blank"><img src ="'.$datas->profile_picture.'"/></a>'.'<br />';
		echo 'Account ID :'.$datas->id.'<br />';
		echo 'Username :'.$datas->username.'<br />';
		echo 'Biografi :'.$datas->bio.'<br />';
		echo 'Website:'.$datas->website.'<br />';
		echo 'Full Name:'.$datas->full_name.'<br />';
		}*/
		}
	function getMediaByMediaId($media_id)
		{
		$json_url = 'https://api.instagram.com/v1/media/' . $media_id . '?access_token=' . $this->access_token . '';
		$json = file_get_contents($json_url);
		$links = json_decode($json, true);
		return $links;
		/*
		$tipe= $links['data']['type'];
		if($tipe=='video'){
		$url = $links['data']['videos']['standard_resolution']['url'];
		} else{
		$url = $links['data']['images']['standard_resolution']['url'];
		}
		echo '<a href = "'.$url.'" target="blank"><img style="width:100px; height:100px;"src ="'.$url.'"/></a>'.'<br />';
		echo 'Total Comment : '.$links['data']['comments']['count'].'<br />';
		echo 'Created On : '.date('Y-m-j H:i:s', $links['data']['created_time']).'<br />';
		echo 'Total like : '.$links['data']['likes']['count'].'<br />';
		foreach ($links['data']['users_in_photo'] as  $usertag) {
		echo 'Users in Photo: '.$usertag['user']['username'].' | ';
		echo 'ID : '.$usertag['user']['id'].' | ';
		echo 'Fullname : '.$usertag['user']['full_name'].' | '.'<br /><br />';
		}
		echo 'Caption : '.$links['data']['caption']['text'].'<br />';
		echo 'Created By : '.$links['data']['caption']['from']['username'].' | ';
		echo 'ID : '.$links['data']['caption']['from']['id'].' | ';
		echo 'Fullname : '.$links['data']['caption']['from']['full_name'].' | ';
		echo '<a href = "'.$links['data']['caption']['from']['profile_picture'].'" target="blank"><img style="width:30px; height:30px;"src ="'.$links['data']['caption']['from']['profile_picture'].'"/></a>'.'<br />';
		*/
		}
	}
?>

