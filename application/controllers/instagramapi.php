
<?php
/*==========================================
Create by : IIM NUR DIANSYAH
Bitbucket : https://bitbucket.org/iimnd/
============================================*/
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Instagramapi extends CI_Controller

	{
	public function __construct()

		{
		parent::__construct();
		$this->load->library('Instagram');
		$this->instagram = new Instagram();
		}
	public function index()

		{
		$this->load->view('index');
		}
	public function deletecomment()

		{
		$comment_id = $_POST['comment_id'];
		$media_id = $_POST['media_id'];
		$data['status'] = $this->instagram->deleteComment($media_id, $comment_id);
		$this->load->view('viewdeletecomment', $data);
		}
	public function unlikepost()

		{
		$media_id = $_POST['media_id'];
		$data['status'] = $this->instagram->unlikePost($media_id);
		$this->load->view('viewdeletecomment', $data);
		}
	public function getcomment()

		{
		$media_id = $_POST['media_id'];
		$data['comment'] = $this->instagram->getComment($media_id);
		$this->load->view('view1', $data);
		}
	public function getmyfollower()

		{
		$data['list_people'] = $this->instagram->getMyFollower();
		$this->load->view('vfollower', $data);
		}
	public function getmyfollowing()

		{
		$data['list_people'] = $this->instagram->getMyFollowing();
		$this->load->view('vfollower', $data);
		}
	public function getlikerbymediaid()

		{
		$media_id = $_POST['media_id'];
		$data['list_people'] = $this->instagram->getLikerByMediaId($media_id);
		$this->load->view('vfollower', $data);
		}
	public function getlocationinfobyid()

		{
		$loc_id = $_POST['loc_id'];
		$data['links'] = $this->instagram->getLocationInfoById($loc_id);
		$this->load->view('viewloc', $data);
		}
	public function getmediabylatlng()

		{
		$lat = $_POST['lat'];
		$lng = $_POST['lng'];
		$data['media'] = $this->instagram->getMediaByLatLng($lat, $lng);
		$this->load->view('viewmedia', $data);
		}
	public function getrecentmediabytag()

		{
		$tags = $_POST['tags'];
		$data['media'] = $this->instagram->getRecentMediaByTag($tags);
		$this->load->view('viewmedia', $data);
		}
	public function getrecentmediabyloc()

		{
		$loc_id = $_POST['loc_id'];
		$data['media'] = $this->instagram->getRecentMediaByLocation($loc_id);
		$this->load->view('viewmedia', $data);
		}
	public function getmyinfo()

		{
		$data['links'] = $this->instagram->getMyInfo();
		$this->load->view('viewuser', $data);
		}
	public function getuserinfobyid()

		{
		$user_id = $_POST['user_id'];
		$data['links'] = $this->instagram->getUserInfoById($user_id);
		$this->load->view('viewuser', $data);
		}
	public function likepost()

		{
		$media_id = $_POST['media_id'];
		$data['status'] = $this->instagram->likePost($media_id);
		$this->load->view('viewdeletecomment', $data);
		}
	public function postcomment()

		{
		$media_id = $_POST['media_id'];
		$comment = $_POST['comment'];
		$data['status'] = $this->instagram->postComment($comment, $media_id);
		$this->load->view('viewdeletecomment', $data);
		}
	public function counttag()

		{
		$tag = $_POST['tag'];
		$data['tag'] = $this->instagram->countTag($tag);
		$this->load->view('tagcount', $data);
		}
	public function searchlocbylatlng()

		{
		$lat = $_POST['lat'];
		$lng = $_POST['lng'];
		$data['location'] = $this->instagram->searchLocationByLatLng($lat, $lng);
		$this->load->view('viewlocation', $data);
		}
	public function searchuser()

		{
		$username = $_POST['username'];
		$data['list_people'] = $this->instagram->searchUser($username);
		$this->load->view('viewfollower', $data); //use same view because object properties are same
		}
	public function getmediabymediaid()

		{
		$media_id = $_POST['media_id'];
		$data['links'] = $this->instagram->getMediaByMediaId($media_id);
		$this->load->view('singlemedia', $data);
		}
	}