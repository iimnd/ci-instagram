

<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta http-equiv="X-UA-Compatible" content="chrome=1">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/index/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/index/easy-responsive-tabs.css " />
        <script src="<?php echo base_url();?>assets/index/jquery-1.9.1.min.js"></script>
        <script src="<?php echo base_url();?>assets/index/easyResponsiveTabs.js"></script>
        <!--[if lt IE 9]>
        <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <title>Instagram API</title>
        <style type="text/css" rel="stylesheet">
            body {
            margin: 0 auto;
            background: #fff;
            }
            .headers {
            width : 100%;
            height :auto;
            background: #438EB9;
            padding-top:8px;
            padding-bottom:10px;
            margin: 0 auto;
            }
            .headers h2 {
            margin-left:40px;
            }
            .footers {
            width : 100%;
            height :auto;
            background: #438EB9;
            padding-top:10px;
            padding-bottom:8px;
            margin: 0 auto;
            }
            .footers h2{
            margin-left:40px;
            }
            .footers a{
            color: #fff;
            }
            #container {
            width: 90%;
            margin: 0 auto;
            }
            @media only screen and (max-width: 768px) {
            #container {
            width: 90%;
            margin: 0 auto;
            }
            }
        </style>
    </head>
    <body>
        <div class="headers">
            <h2>INSTAGRAM API </h2>
        </div>
        <div id="container">
            <br/>
            <br/>
            <br/>
            <br/>
            <!--Vertical Tab-->
            <div id="parentVerticalTab">
                <ul class="resp-tabs-list hor_1">
                    <li>Delete Comment</li>
                    <li>Show Comment</li>
                    <li>Unlike Post</li>
                    <li>List Follower</li>
                    <li>List Following</li>
                    <li>Get Liker By Media ID</li>
                    <li>Get Media By LatLng</li>
                    <li>Location Info By ID</li>
                    <li>Get Media By tag</li>
                    <li>Get Media By Loc ID</li>
                    <li>View My Info</li>
                    <li>View User By ID </li>
                    <li>Like Post </li>
                    <li>Post Comment </li>
                    <li>Tag Count</li>
                    <li>Search Location By Lat Lng </li>
                    <li>Search User</li>
                    <li>Get Media By Media ID</li>
                
                </ul>
                <div class="resp-tabs-container hor_1">

                 <!-- Contact Put starts here --> 
                    <div>
                        <form class="form-vertical" method="post" action="<?php echo base_url().'index.php/instagramapi/deletecomment';?>" target="_blank" >
                            <!-- Form Name -->
                            <legend>Delete Comment</legend>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" >Media ID</label>  
                                <div class="col-md-4">
                                    <input id="media_id" name="media_id" placeholder="Media ID" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="cid">Comment ID</label>  
                                <div class="col-md-4">
                                    <input id="comment_id" name="comment_id" placeholder="Comment ID" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                            
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="limitButton"></label>
                                <div class="col-md-4">
                                    <button type ="submit" id="limitButton" name="limitButton" class="btn btn-primary">Delete Comment</button>
                                </div>
                            </div>
                        </form>
                        <br>
                        <br>
                    </div>
                     <!-- Contact Put ends here --> 






                      <!-- Contact Update starts here --> 
                    <div>
                        <form class="form-vertical" method="post"  action="<?php echo base_url().'index.php/instagramapi/getcomment';?>" target="_blank">
                            <!-- Form Name -->
                            <legend>Get Comment</legend>
                             <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" >Media ID</label>  
                                <div class="col-md-4">
                                    <input id="media_id" name="media_id" placeholder="Media ID" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                            
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="limitButton"></label>
                                <div class="col-md-4">
                                    <button type ="submit" id="limitButton" name="limitButton" class="btn btn-primary">Show Comment</button>
                                </div>
                            </div>
                        </form>
                        <br>
                        <br>
                    </div>
                    <!-- Contact Update ends here --> 







                     <!-- Contact delete starts here --> 
                    <div>
                        <form class="form-vertical" method="post"  action="<?php echo base_url().'index.php/instagramapi/unlikepost/';?>" target="_blank">
                            <!-- Form Name -->
                            <legend>Unlike Post</legend>
                            <div class="form-group">
                                <label class="col-md-4 control-label" >Media ID</label>  
                                <div class="col-md-4">
                                    <input id="media_id" name="media_id" placeholder="Media ID" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="limitButton"></label>
                                <div class="col-md-4">
                                    <button type ="submit" id="limitButton" name="limitButton" class="btn btn-primary">Unlike</button>
                                </div>
                            </div>
                        </form>
                        <br>
                        <br>
                    </div>
                     <!-- Contact delete ends here --> 






                        <!-- Contact Search starts here --> 
                    <div>
                        <form class="form-vertical" method="get"  action="<?php echo base_url().'index.php/instagramapi/getmyfollower/';?>" target="_blank">
                            <!-- Form Name -->
                            <legend>List Follower</legend>
                           
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="limitButton"></label>
                                <div class="col-md-4">
                                    <button type ="submit" id="limitButton" name="limitButton" class="btn btn-primary">Show List</button>
                                </div>
                            </div>
                        </form>
                        <br>
                        <br>
                    </div>
                    <!-- Contact Search ends here --> 







                      <!-- Product Put starts here --> 
                    <div>
                        <form class="form-vertical" method="post"  action="<?php echo base_url().'index.php/instagramapi/getmyfollowing/';?>" target="_blank">
                            <!-- Form Name -->
                            <legend>List Following</legend>
                            
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="limitButton"></label>
                                <div class="col-md-4">
                                    <button type ="submit" id="limitButton" name="limitButton" class="btn btn-primary">Show List</button>
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>
                     <!-- Product Put ends here --> 






                      <!-- Product Update starts here --> 
                    <div>
                        <form class="form-vertical" method="post"  action="<?php echo base_url().'index.php/instagramapi/getlikerbymediaid/';?>" target="_blank">
                            <!-- Form Name -->
                            <legend>Get Liker By Media ID</legend>
                            <div class="form-group">
                                <label class="col-md-4 control-label" >Media ID</label>  
                                <div class="col-md-4">
                                    <input id="media_id" name="media_id" placeholder="Media ID" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                           
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="limitButton"></label>
                                <div class="col-md-4">
                                    <button type ="submit" id="limitButton" name="limitButton" class="btn btn-primary">Show</button>
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>
                     <!-- Product Update ends here --> 






                      <!-- Product Delete starts here --> 
                    <div>
                        <form class="form-vertical" method="post"  action="<?php echo base_url().'index.php/instagramapi/getmediabylatlng/';?>" target="_blank">
                            <!-- Form Name -->
                            <legend>Get Media By LatLng</legend>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="latitude">Latitude</label>  
                                <div class="col-md-4">
                                    <input id="lat" name="lat" placeholder="latitude" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-md-4 control-label" for="longitude">Longitude</label>  
                                <div class="col-md-4">
                                    <input id="lng" name="lng" placeholder="longitude" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                            
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="limitButton"></label>
                                <div class="col-md-4">
                                    <button type ="submit" id="limitButton" name="limitButton" class="btn btn-primary">Show Media</button>
                                </div>
                            </div>
                        </form>
                    </div>
                     <!-- Product Delete ends here --> 


  <!-- Product Put starts here --> 
                    <div>
                        <form class="form-vertical" method="post"  action="<?php echo base_url().'index.php/instagramapi/getlocationinfobyid/';?>" target="_blank">
                            <!-- Form Name -->
                            <legend>Location Info By ID</legend>
                             <div class="form-group">
                                <label class="col-md-4 control-label" for="loc_id">Location ID</label>  
                                <div class="col-md-4">
                                    <input id="loc_id" name="loc_id" placeholder="Location ID" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="limitButton"></label>
                                <div class="col-md-4">
                                    <button type ="submit" id="limitButton" name="limitButton" class="btn btn-primary">Show Location</button>
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>
                     <!-- Product Put ends here --> 


                       <!-- Product Search starts here --> 
                    <div>
                       <form class="form-vertical" method="post"  action="<?php echo base_url().'index.php/instagramapi/getrecentmediabytag/';?>" target="_blank">
                            <!-- Form Name -->
                            <legend>Get Media By Tag</legend>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="tag">Tag</label>  
                                <div class="col-md-4">
                                    <input id="tags" name="tags" placeholder="Tag" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                           
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="limitButton"></label>
                                <div class="col-md-4">
                                    <button type ="submit" id="limitButton" name="limitButton" class="btn btn-primary">Show Media</button>
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>
                     <!-- Product Search ends here --> 




                    <div>
                        <form class="form-vertical" method="post"  action="<?php echo base_url().'index.php/instagramapi/getrecentmediabyloc/';?>" target="_blank">
                            <!-- Form Name -->
                            <legend>Location Info By ID</legend>
                             <div class="form-group">
                                <label class="col-md-4 control-label" for="loc_id">Location ID</label>  
                                <div class="col-md-4">
                                    <input id="loc_id" name="loc_id" placeholder="Location ID" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="limitButton"></label>
                                <div class="col-md-4">
                                    <button type ="submit" id="limitButton" name="limitButton" class="btn btn-primary">Show Media</button>
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>






                   <div>
                        <form class="form-vertical" method="post"  action="<?php echo base_url().'index.php/instagramapi/getmyinfo/';?>" target="_blank">
                            <!-- Form Name -->
                            <legend>View My Info</legend>
                            
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="limitButton"></label>
                                <div class="col-md-4">
                                    <button type ="submit" id="limitButton" name="limitButton" class="btn btn-primary">Show Info</button>
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>







                   <div>
                        <form class="form-vertical" method="post"  action="<?php echo base_url().'index.php/instagramapi/getuserinfobyid/';?>" target="_blank">
                            <!-- Form Name -->
                            <legend>Show User By ID</legend>
                             <div class="form-group">
                                <label class="col-md-4 control-label" for="user_id">User ID</label>  
                                <div class="col-md-4">
                                    <input id="user_id" name="user_id" placeholder="User ID" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="limitButton"></label>
                                <div class="col-md-4">
                                    <button type ="submit" id="limitButton" name="limitButton" class="btn btn-primary">Show User Info</button>
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>




                      <!-- Product Search starts here --> 
                    <div>
                       <form class="form-vertical" method="post"  action="<?php echo base_url().'index.php/instagramapi/likepost/';?>" target="_blank">
                            <!-- Form Name -->
                            <legend>Like Post</legend>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="media_id">Media ID</label>  
                                <div class="col-md-4">
                                    <input id="media_id" name="media_id" placeholder="Media ID" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                           
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="limitButton"></label>
                                <div class="col-md-4">
                                    <button type ="submit" id="limitButton" name="limitButton" class="btn btn-primary">Like Post Now</button>
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>
                     <!-- Product Search ends here --> 



 <!-- Product Search starts here --> 
                    <div>
                       <form class="form-vertical" method="post"  action="<?php echo base_url().'index.php/instagramapi/postcomment/';?>" target="_blank">
                            <!-- Form Name -->
                            <legend>Post Komentar</legend>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="media_id">Media ID</label>  
                                <div class="col-md-4">
                                    <input id="media_id" name="media_id" placeholder="Media ID" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                              <div class="form-group">
                                <label class="col-md-4 control-label" for="comment">Komentar</label>  
                                <div class="col-md-4">
                                    <input id="comment" name="comment" placeholder="Komentar" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                           
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="limitButton"></label>
                                <div class="col-md-4">
                                    <button type ="submit" id="limitButton" name="limitButton" class="btn btn-primary">Post Comment</button>
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>
                     <!-- Product Search ends here --> 


 <div>
                       <form class="form-vertical" method="post"  action="<?php echo base_url().'index.php/instagramapi/counttag/';?>" target="_blank">
                            <!-- Form Name -->
                            <legend>Tag Count</legend>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="tag">Tag</label>  
                                <div class="col-md-4">
                                    <input id="tag" name="tag" placeholder="Tag" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                              
                           
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="limitButton"></label>
                                <div class="col-md-4">
                                    <button type ="submit" id="limitButton" name="limitButton" class="btn btn-primary">Show</button>
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>



<div>
                       <form class="form-vertical" method="post"  action="<?php echo base_url().'index.php/instagramapi/searchlocbylatlng/';?>" target="_blank">
                            <!-- Form Name -->
                            <legend>Search Location</legend>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="lat">Latitude</label>  
                                <div class="col-md-4">
                                    <input id="lat" name="lat" placeholder="Latitude" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                              
                             <div class="form-group">
                                <label class="col-md-4 control-label" for="lng">Longitude</label>  
                                <div class="col-md-4">
                                    <input id="lng" name="lng" placeholder="Longitude" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                              
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="limitButton"></label>
                                <div class="col-md-4">
                                    <button type ="submit" id="limitButton" name="limitButton" class="btn btn-primary">Search</button>
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>





                    <div>
                       <form class="form-vertical" method="post"  action="<?php echo base_url().'index.php/instagramapi/searchuser/';?>" target="_blank">
                            <!-- Form Name -->
                            <legend>Search User</legend>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="username">Username</label>  
                                <div class="col-md-4">
                                    <input id="username" name="username" placeholder="Username" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                              
                           
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="limitButton"></label>
                                <div class="col-md-4">
                                    <button type ="submit" id="limitButton" name="limitButton" class="btn btn-primary">Search</button>
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>





                        <!-- Product Search starts here --> 
                    <div>
                       <form class="form-vertical" method="post"  action="<?php echo base_url().'index.php/instagramapi/getmediabymediaid/';?>" target="_blank">
                            <!-- Form Name -->
                            <legend>Like Post</legend>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="media_id">Media ID</label>  
                                <div class="col-md-4">
                                    <input id="media_id" name="media_id" placeholder="Media ID" class="form-control input-md" type="text">
                                    <br>
                                </div>
                            </div>
                           
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="limitButton"></label>
                                <div class="col-md-4">
                                    <button type ="submit" id="limitButton" name="limitButton" class="btn btn-primary">Show Media</button>
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>
                     <!-- Product Search ends here --> 



                </div>
            </div>
            <br>
            <br>
            <br/>
            <br>
            <br>
            <br/>
        </div>
        <!--Plug-in Initialisation-->
        <script type="text/javascript">
            $(document).ready(function() {
                //Horizontal Tab
                $('#parentHorizontalTab').easyResponsiveTabs({
                    type: 'default', //Types: default, vertical, accordion
                    width: 'auto', //auto or any width like 600px
                    fit: true, // 100% fit in a container
                    tabidentify: 'hor_1', // The tab groups identifier
                    activate: function(event) { // Callback function if tab is switched
                        var $tab = $(this);
                        var $info = $('#nested-tabInfo');
                        var $name = $('span', $info);
                        $name.text($tab.text());
                        $info.show();
                    }
                });
            
                // Child Tab
                $('#ChildVerticalTab_1').easyResponsiveTabs({
                    type: 'vertical',
                    width: 'auto',
                    fit: true,
                    tabidentify: 'ver_1', // The tab groups identifier
                    activetab_bg: '#fff', // background color for active tabs in this group
                    inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
                    active_border_color: '#c1c1c1', // border color for active tabs heads in this group
                    active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
                });
            
                //Vertical Tab
                $('#parentVerticalTab').easyResponsiveTabs({
                    type: 'vertical', //Types: default, vertical, accordion
                    width: 'auto', //auto or any width like 600px
                    fit: true, // 100% fit in a container
                    closed: 'accordion', // Start closed if in accordion view
                    tabidentify: 'hor_1', // The tab groups identifier
                    activate: function(event) { // Callback function if tab is switched
                        var $tab = $(this);
                        var $info = $('#nested-tabInfo2');
                        var $name = $('span', $info);
                        $name.text($tab.text());
                        $info.show();
                    }
                });
            });
        </script>
    </body>
</html>

